//
//  AutoHeightTableView.swift
//  CurrencyConverter
//
//  Created by kamrul hasan on 17/6/19.
//  Copyright © 2019 hasan. All rights reserved.
//

import UIKit

class AutoHeightTableView: UITableView {
    
    var maxSize: CGSize = .zero
    
    override var contentSize: CGSize {
        didSet { invalidateIntrinsicContentSize() }
    }
    
    override var intrinsicContentSize: CGSize {
        var size = contentSize
        if maxSize.width > 0 && size.width > maxSize.width {
            size.width = maxSize.width
        }
        if maxSize.height > 0 && size.height > maxSize.height {
            size.height = maxSize.height
        }
        return size
    }
}
