//
//  CurrencyViewModel.swift
//  CurrencyConverter
//
//  Created by kamrul hasan on 16/6/19.
//  Copyright © 2019 hasan. All rights reserved.
//

import Foundation
import Alamofire
import Marshal
import SwiftyJSON

class CurrencyViewModel: CurrencyViewModelProtocol {
    var delegate: CurrencyViewModelDelegate?
    var currencyList: [RateModel] = []
    
    func getData() {
        let url = RestAPI.sharedInstancde.urlBody
        delegate?.startFetchingData()
        
        Alamofire.request(url, method: .get).responseJSON { (response) in
            print(response)
            if response.result.value != nil {
                switch response.result{
                case .failure(_):
                    self.delegate?.dataLoadingFailed()
                case .success(let value):
                    let json = JSON(value)
                    var array: [RateModel] = []
                    if let arrayResult = json["rates"].array{
                        arrayResult.forEach({ (json) in
                            array.append(RateModel(json))
                        })
                    }
                    
                    
                    self.currencyList = array
                    // self.list.append(MVVMCListModel())
                    self.delegate?.dataLoadingSuccess()
                }
            }
            else {
                print("Data not found")
            }
        }
    }
    
    func getRateByCountry(countryName: String) ->  RateModel.Periods.RateValue?  {
        let sc:[RateModel] =  currencyList.filter { (m) -> Bool in
            m.name == countryName
        }
        if sc.count > 0 {
            let countyRate: RateModel = sc[0]
            if countyRate.periods.count > 0 {
                let period: RateModel.Periods = countyRate.periods[0]
                if  let periodRate: RateModel.Periods.RateValue = period.rate {
                   return periodRate
                }
            }
        }
        return nil
    }
    
}
