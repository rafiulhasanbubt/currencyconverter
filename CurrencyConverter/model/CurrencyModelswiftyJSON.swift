//
//  CurrencyModelswiftyJSON.swift
//  CurrencyConverter
//
//  Created by kamrul hasan on 16/6/19.
//  Copyright © 2019 hasan. All rights reserved.
//

import Foundation
import SwiftyJSON

//class CurrencyModelswiftyJSON{
   // let details: String?
   // let version: String?
   // let rates: [Rates] = []
    
    class RateModel{
        var name: String? = nil
        var code : String? = nil
        var country_code : String? = nil
        var periods: [Periods] = []
        
        class Periods{
           // var effective_from: String? = nil
            var rate: RateValue?
            class RateValue{
                var super_reduced: Int? = nil
                var reduced: Int? = nil
                var standard : Int? = nil
                var reduced1: Int? = nil
                var reduced2: Int? = nil
                var parking: Int? = nil
                
                init(_ json: JSON) {
                    super_reduced = json["super_reduced"].int
                    reduced = json["reduced"].int
                    standard = json["standard"].int
                    reduced1 = json["reduced1"].int
                    reduced2 = json["reduced2"].int
                    parking = json["parking"].int
                }
            }
            
            init(_ json: JSON) {
               // effective_from = json["effective_from"].string
                rate = RateValue(json["rates"])
            }
        }
        
        init(_ json: JSON) {
            name = json["name"].string
            code = json["code"].string
            country_code = json["country_code"].string
           // periods =  json["periods"].arrayObject as? [RateModel.Periods] ?? []//Periods(json["periods"])
            var array: [Periods] = []
            if let arrayResult = json["periods"].array{
                arrayResult.forEach({ (pJson) in
                    array.append(Periods(pJson))
                })
            }
            periods = array
        }
    }
   /*
    init(_ json: JSON) {
      //  details = json["details"].string
       // version = json["version"].string
        rates = json["rates"].array
        //Rates(json["rates"])
    }*/
//}

