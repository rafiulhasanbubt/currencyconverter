//
//  CurrencyModel.swift
//  CurrencyConverter
//
//  Created by kamrul hasan on 16/6/19.
//  Copyright © 2019 hasan. All rights reserved.
//

import Foundation
//import SwiftyJSON
import Marshal

class CurrencyModel: Unmarshaling{
    let details: String?
    let version: String?
    let rates: Rates?
    
    class Rates: Unmarshaling {
        let name: String?
        let code: String?
        let countryCode: String?
        let periods: Periods?
        
        class Periods: Unmarshaling {
            
            let effectiveFrom: String?
            let rate: Rate?
            
            class Rate: Unmarshaling{
                let superReduced: Int?
                let reduced: Int?
                let standard: Int?
                
                required init(object: MarshaledObject) throws {
                    superReduced = try object.value(for: "super_reduced")
                    reduced = try object.value(for: "reduced")
                    standard = try object.value(for: "standard")
                }
            }
            
            required init(object: MarshaledObject) throws {
                effectiveFrom = try object.value(for: "effective_from")
                rate = try object.value(for: "rates")
            }
        }
        
        required init(object: MarshaledObject) throws {
            name = try object.value(for: "name")
            code = try object.value(for: "code")
            countryCode = try object.value(for: "country_code")
            periods = try object.value(for: "periods")
        }
        
    }
    
    required init(object: MarshaledObject) throws {
        details = try object.value(for: "details")
        version = try object.value(for: "version")
        rates = try object.value(for: "rates")
    }
}
