//
//  RateCell.swift
//  CurrencyConverter
//
//  Created by kamrul hasan on 17/6/19.
//  Copyright © 2019 hasan. All rights reserved.
//

import UIKit

class RateCell: UITableViewCell {

    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var radioImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
