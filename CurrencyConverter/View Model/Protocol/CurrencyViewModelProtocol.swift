//
//  CurrencyViewModelProtocol.swift
//  CurrencyConverter
//
//  Created by kamrul hasan on 16/6/19.
//  Copyright © 2019 hasan. All rights reserved.
//

import Foundation
import  UIKit

protocol CurrencyViewModelDelegate: class {
    func startFetchingData()
    func dataLoadingFailed()
    func dataLoadingSuccess()
}

protocol CurrencyViewModelProtocol {
    var delegate: CurrencyViewModelDelegate? { get set }
    var currencyList: [RateModel] { get }
    func getData()
    func getRateByCountry(countryName: String) ->  RateModel.Periods.RateValue? 
}
