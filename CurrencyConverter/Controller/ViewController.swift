//
//  ViewController.swift
//  CurrencyConverter
//
//  Created by kamrul hasan on 16/6/19.
//  Copyright © 2019 hasan. All rights reserved.
//

import UIKit
import CountryPickerView
import DLRadioButton
import IQKeyboardManager

var standardValue: Double = 0.0
var superReducedValue:Double = 0.0
var reducedValue:Double = 0.0
var reduced1Value: Double = 0.0
var reduced2Value: Double = 0.0
var parkingValue: Double = 0.0

var originalValue:Double = 0.0
var taxValue: Double = 0.0
var totalValue: Double = 0.0

var dic:Dictionary<String,Double> = [:]

class ViewController: UIViewController{
    @IBOutlet weak var tableView: UITableView!
    
    let cellReuseIdentifier = "currencyCell"

    @IBOutlet weak var countryPickerView: CountryPickerView!
    @IBOutlet weak var standardButton: DLRadioButton!
    
    // calculation 
    @IBOutlet weak var originalValueLabel: UILabel!
    @IBOutlet weak var taxValueLabel: UILabel!
    @IBOutlet weak var totalValueLabel: UILabel!
    
    //currencyTextfield
    @IBOutlet weak var currencyTextfield: UITextField!
    
    // view model
    var viewModel : CurrencyViewModelProtocol?{
        willSet{
            viewModel?.delegate = nil
        }
        didSet{
            viewModel?.delegate = self
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewModel = CurrencyViewModel()
        viewModel?.getData()
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "RateCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.scrollsToTop = false
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 0
        

        //country picker
        countryPickerView.setCountryByName("Bangladesh")
        countryPickerView.delegate = self
        countryPickerView.dataSource = self
        countryPickerView.showPhoneCodeInView = false
        countryPickerView.showCountryCodeInView = false
        countryPickerView.flagImageView.isHidden = true
        countryPickerView.flagSpacingInView = 0.0
        countryPickerView.countryDetailsLabel.text = countryPickerView.selectedCountry.name
        
        // get textfield value after clicking done instead of return button from keyboard
        currencyTextfield.returnKeyType = UIReturnKeyType.done

    }
}

extension ViewController : CountryPickerViewDataSource, CountryPickerViewDelegate {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        let selectedCountry = countryPickerView.selectedCountry.name
        countryPickerView.countryDetailsLabel.text = selectedCountry
        print(selectedCountry)
        
        let rateM = viewModel?.getRateByCountry(countryName: selectedCountry)

        standardValue = Double(rateM?.standard ?? 0)
        if standardValue != 0 {
            print(standardValue)
            dic.updateValue(standardValue, forKey: "standard")
        }
        
        superReducedValue = Double(rateM?.super_reduced ?? 0)
        if superReducedValue != 0 {
            print(superReducedValue)
            dic.updateValue(superReducedValue, forKey: "super_reduced")
        }
        
        reducedValue = Double(rateM?.reduced ?? 0)
        if reducedValue != 0 {
            print(reducedValue)
            dic.updateValue(reducedValue, forKey: "reduced")
        }
        
        reduced1Value = Double(rateM?.reduced1 ?? 0)
        if reduced1Value != 0 {
            print(reduced1Value)
            dic.updateValue(reduced1Value, forKey: "reduced1")
        }
        
        reduced2Value = Double(rateM?.reduced2 ?? 0)
        if reduced2Value != 0 {
            print(reduced2Value)
            dic.updateValue(reduced2Value, forKey: "reduced2")
            print(dic)
        }
        
        parkingValue = Double(rateM?.parking ?? 0)
        if parkingValue != 0 {
            print(parkingValue)
            dic.updateValue(parkingValue, forKey: "parking")
            print(dic)
        }
        print(dic)
       
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

}
    
extension ViewController: UITableViewDelegate, UITableViewDataSource  {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dic.keys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! RateCell
        
        let key   = Array(dic.keys)[indexPath.row]
        let value = Array(dic.values)[indexPath.row]
        
        // set the text from the data model
        //cell.textLabel?.text = key + " (" + "\(value)%)"
        cell.rateLabel.text = key + " (" + "\(value)%)"
        cell.radioImageView.image = #imageLiteral(resourceName: "Radio-Off.pdf")
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 24
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = self.tableView.cellForRow(at: indexPath) as! RateCell
        cell.radioImageView.image = #imageLiteral(resourceName: "RadioOn.pdf")
            //let key   = Array(dic.keys)[indexPath.row]
            let value = Array(dic.values)[indexPath.row]
            getCurrencyValue()
            let finalTax = originalValue / value
            let finalTaxText = String(format: "%.2f", finalTax)
            taxValueLabel.text = finalTaxText
        
            let total = originalValue + finalTax
            let totalText = String(format: "%.2f", total)
            totalValueLabel.text = totalText
  }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = self.tableView.cellForRow(at: indexPath) as! RateCell
        cell.radioImageView.image = #imageLiteral(resourceName: "Radio-Off.pdf")
    }
    
}

extension ViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        originalValueLabel.text = textField.text
        let getOriginalValue = originalValueLabel.text ?? ""
        originalValue = Double(getOriginalValue)!
        totalValueLabel.text = ""
        taxValueLabel.text = ""
        tableView.reloadData()
    }
    
    func getCurrencyValue() {
        totalValue = originalValue + taxValue
        totalValueLabel.text = String(format: "%.2f", totalValue)
    }
}


extension ViewController: CurrencyViewModelDelegate{
    func startFetchingData() {
        print("data loading")
    }
    
    func dataLoadingFailed() {
        print("data loading failed")
    }
    
    func dataLoadingSuccess() {
        print("success")
    }
}
